class Item < ApplicationRecord
	attr_accessor :image_full
    
  def self.create_items_by(item_params)


  return false if item_params[:image].nil?

  Item.transaction do 

    item_params[:image].each do |item|
      new_item = Item.new(item: item_params[:item], image: item)
      return false unless new_item.save!
    end
  end

  true
end


end

