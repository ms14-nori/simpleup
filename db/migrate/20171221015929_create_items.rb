class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :item
      t.binary :image

      t.timestamps
    end
  end
end
